package com.id.taqi.hellospring.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.id.taqi.hellospring.model.Karyawan;

public interface KaryawanRepository extends PagingAndSortingRepository<Karyawan, String>{

}
