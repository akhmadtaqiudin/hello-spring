package com.id.taqi.hellospring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity @Data
public class Karyawan {

	@Id @GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid", strategy="uuid2")
	private String nik;
	
	@NotNull @NotEmpty @Size(max = 100)
	private String nama;
	
	@NotNull @NotEmpty @Size(max = 100)
	private String bagian;
	
	@NotNull @NotEmpty @Size(max = 100)
	private String cabang;
}
