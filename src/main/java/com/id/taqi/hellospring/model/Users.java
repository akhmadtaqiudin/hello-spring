package com.id.taqi.hellospring.model;

public class Users {

	private String userName;
	private Long salary;
	public Users(String userName, Long salary) {
		super();
		this.userName = userName;
		this.salary = salary;
	}
	public Users() {
		
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getSalary() {
		return salary;
	}
	public void setSalary(Long salary) {
		this.salary = salary;
	}
	
}
