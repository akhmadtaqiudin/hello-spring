package com.id.taqi.hellospring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class TestRest {

	@GetMapping("/hello")
	public String hello(){
		return "Hello Spring";
	}
	
	@PostMapping("/post")
	public String helloPost(@RequestBody final String hello){
		return "Hello Spring";
	}
	
	@PutMapping("/put")
	public String helloPut(@RequestBody final String hello){
		return "Hello Spring";
	}
}
