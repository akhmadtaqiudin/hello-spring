package com.id.taqi.hellospring.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.id.taqi.hellospring.model.Users;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/rest/user")
public class UserController {

	@GetMapping @ApiOperation("Menampilkan Semua Data User")
	public List<Users> getAllUser(){
		return Arrays.asList(new Users("qwe",1110L), new Users("asd",1230L));
	}
	
	@GetMapping("/{userName}") @ApiOperation("Menampilkan Data User Berdasarkan userName")
	public Users getUser(@PathVariable("userName") final String userName){
		return new Users(userName, 1230L);
	}
}
