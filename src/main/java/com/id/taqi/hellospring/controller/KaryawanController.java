package com.id.taqi.hellospring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.id.taqi.hellospring.model.Karyawan;
import com.id.taqi.hellospring.repository.KaryawanRepository;

@RestController @RequestMapping("/api/karyawan")
public class KaryawanController {

	@Autowired
	private KaryawanRepository karyawanRepository;
	
	@GetMapping("/")
	public Page<Karyawan> findKaryawan(Pageable page){
		return karyawanRepository.findAll(page);
	}
}
